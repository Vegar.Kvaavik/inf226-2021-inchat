# INCHAT – The INsecure CHAT application

## Task 0 – Authentication (3 points)

Implemented scrypt such that passwords are not stored in plaintext

Created classes `UserName` and `Password`. Was not able to replace String with these classes.
Implemented some methods:
- Password length
- Check password against **rockyou** wordlist
- Check that repeated password matches
- Check that username length >= 6
- Check that username is not equal password

Implementations I was not able to do:
- Users can not register with identical usernames. 
- Avoid consecutive/sequential/context-specific passwords
- 


## Task 1 – SQL injection (3 points)

Implemented prepared statements

## Task 2 – Cross-site scripting (3 points)

Added httponly to cookie and sanitize user input

Cons:
- Different characters is HTML-encoded and therefore not so pretty when printed to the screen.


## Task 3 – Cross-site request forgery (1 point)

Added secure flag and samesite=strict to cookie

## Task 4 – Access control (3 points)

Started on a simple solution that saves roles (owner, banned) into a list.\
This is however not usable since the list gets deleted when the servers restarts

## Task ω – Other security holes (2 points)

ZAP reported vulnerabilities regarding headers: x-frame-options and x-content-type-options.\
Added deny and nosniff to these

Removed the debug channel as this was a vulnerability as an attacker could query the database


## Other

Implementations I was not able to do:
- Avoid that user can create identical channel names
- Because of error message I could not use PrintWriter more than once to print to the page.\
I wanted to let the user know when and why password/username was not valid
- I tried to update the outdated dependencies in pom.xml, but no success