package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;
import java.util.function.Consumer;

import inf226.storage.*;
import inf226.util.*;




public final class EventStorage
    implements Storage<Channel.Event,SQLException> {
    
    private final Connection connection;
    
    public EventStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Event (id TEXT PRIMARY KEY, version TEXT, channel TEXT, type INTEGER, time TEXT, FOREIGN KEY(channel) REFERENCES Channel(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Message (id TEXT PRIMARY KEY, sender TEXT, content Text, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS Joined (id TEXT PRIMARY KEY, sender TEXT, FOREIGN KEY(id) REFERENCES Event(id) ON DELETE CASCADE)");
    }
    
    @Override
    public Stored<Channel.Event> save(Channel.Event event)
      throws SQLException {
        
        final Stored<Channel.Event> stored = new Stored<Channel.Event>(event);

        String sql = "INSERT INTO Event VALUES(?,?,?,?,?)";
        PreparedStatement saveEvent = connection.prepareStatement(sql);
        saveEvent.setObject(1, stored.identity);
        saveEvent.setObject(2, stored.version);
        saveEvent.setObject(3, event.channel);
        saveEvent.setInt(4, event.type.code);
        saveEvent.setObject(5, event.time);
        saveEvent.executeUpdate();

        switch (event.type) {
            case message:
                sql = "INSERT INTO Message VALUES(?,?,?)";
                PreparedStatement eventMessage = connection.prepareStatement(sql);
                eventMessage.setObject(1, stored.identity);
                eventMessage.setString(2, event.sender);
                eventMessage.setString(3, event.message);
                eventMessage.executeUpdate();
                break;
            case join:
                sql = "INSERT INTO Joined VALUES(?,?)";
                eventMessage = connection.prepareStatement(sql);
                eventMessage.setObject(1, stored.identity);
                eventMessage.setString(2, event.sender);
                eventMessage.executeUpdate();
                break;
        }
        return stored;
    }
    
    @Override
    public synchronized Stored<Channel.Event> update(Stored<Channel.Event> event,
                                            Channel.Event new_event)
        throws UpdatedException,
            DeletedException,
            SQLException {
    final Stored<Channel.Event> current = get(event.identity);
    final Stored<Channel.Event> updated = current.newVersion(new_event);
    if(current.version.equals(event.version)) {
        String sql = "UPDATE Event SET (version,channel,time,type) = (?,?,?,?) WHERE id=?";
        PreparedStatement updateEvent = connection.prepareStatement(sql);
        updateEvent.setObject(1, updated.version);
        updateEvent.setObject(2, new_event.channel);
        updateEvent.setObject(3, new_event.time);
        updateEvent.setInt(4, new_event.type.code);
        updateEvent.setObject(5, updated.identity);
        updateEvent.executeUpdate();

        switch (new_event.type) {
            case message:
                sql = "UPDATE Message SET (sender,content)=(?,?) WHERE id=?";
                PreparedStatement eventMessage = connection.prepareStatement(sql);
                eventMessage.setString(1, new_event.sender);
                eventMessage.setString(2, new_event.message);
                eventMessage.setObject(3, updated.identity);
                eventMessage.executeUpdate();
                break;
            case join:
                sql = "UPDATE Joined SET (sender)=(?) WHERE id=?";
                eventMessage = connection.prepareStatement(sql);
                eventMessage.setString(1, new_event.sender);
                eventMessage.setObject(2, updated.identity);
                eventMessage.executeUpdate();
                break;
        }

    } else {
        throw new UpdatedException(current);
    }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<Channel.Event> event)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<Channel.Event> current = get(event.identity);
        if(current.version.equals(event.version)) {
            String sql = "DELETE FROM Event WHERE id = ?";
            PreparedStatement deleteEvent = connection.prepareStatement(sql);
            deleteEvent.setObject(1, event.identity);
            deleteEvent.executeUpdate();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<Channel.Event> get(UUID id)
      throws DeletedException,
             SQLException {
        final String sql = "SELECT version,channel,time,type FROM Event WHERE id = ?";
        PreparedStatement getEvent = connection.prepareStatement(sql);
        getEvent.setString(1, id.toString());
        final ResultSet rs = getEvent.executeQuery();

        if(rs.next()) {
            final UUID version = UUID.fromString(rs.getString("version"));
            final UUID channel = 
                UUID.fromString(rs.getString("channel"));
            final Channel.Event.Type type = 
                Channel.Event.Type.fromInteger(rs.getInt("type"));
            final Instant time = 
                Instant.parse(rs.getString("time"));

            switch(type) {
                case message:
                    final String msql = "SELECT sender,content FROM Message WHERE id = ?";
                    PreparedStatement mstatement = connection.prepareStatement(msql);
                    mstatement.setString(1, id.toString());
                    final ResultSet mrs = mstatement.executeQuery();
                    mrs.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createMessageEvent(channel,time,mrs.getString("sender"),mrs.getString("content")),
                            id,
                            version);
                case join:
                    final String asql = "SELECT sender FROM Joined WHERE id = ?";
                    mstatement = connection.prepareStatement(asql);
                    mstatement.setString(1, id.toString());
                    final ResultSet ars = mstatement.executeQuery();
                    ars.next();
                    return new Stored<Channel.Event>(
                            Channel.Event.createJoinEvent(channel,time,ars.getString("sender")),
                            id,
                            version);
            }
        }
        throw new DeletedException();
    }
    
}


 
