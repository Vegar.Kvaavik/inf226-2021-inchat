package inf226.inchat;



public class UserName {

    private String username;

    public UserName(String name) {
            this.username=name;
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public static boolean check(String name) {
        return name.length() >= 6;

    }
}
