package inf226.inchat;
import org.apache.commons.lang3.Validate.*;
/*
Password criteria:
- at least 8 characters in length
- up to at least 64 characters
- All ASCII/Unicode characters should be allowed, including emojis and spaces
- compared against password breach databases and rejected if there’s a match
- Users should be prevented from using sequential (ex. “1234”) or repeated (ex. “aaaa”) characters
- Context-specific words, such as the name of the service, the user’s username, etc. should not be permitted
 */

import javax.servlet.ServletOutputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Instant;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.apache.commons.lang3.Validate.matchesPattern;
import static org.apache.commons.lang3.Validate.notNull;

public class Password {

    public String password;

    public Password(String password) {
        notNull(password);
        this.password = password;
    }

    public static boolean checkLength(String password) {
        return password.length() >= 8 && password.length() <= 64;

    }

    public static boolean leaked(String password) {
        try (FileReader fileInvc = new FileReader("rockyou.txt");
             BufferedReader readervc = new BufferedReader(fileInvc)) {
            String readvc = readervc.readLine();
            while (readvc != null) {
                if (readvc.contains(password)) {
                    return true;
                }
                readvc = readervc.readLine();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }


}
