package inf226.inchat;

import java.sql.*;
import java.time.Instant;
import java.util.UUID;

import inf226.storage.*;
import inf226.util.*;



/**
 * The UserStore stores User objects in a SQL database.
 */
public final class UserStorage
    implements Storage<User,SQLException> {
    
    final Connection connection;
    
    public UserStorage(Connection connection) 
      throws SQLException {
        this.connection = connection;
        connection.createStatement()
                .executeUpdate("CREATE TABLE IF NOT EXISTS User (id TEXT PRIMARY KEY, version TEXT, name TEXT, joined TEXT)");
    }
    
    @Override
    public Stored<User> save(User user)
      throws SQLException {
        final Stored<User> stored = new Stored<>(user);
        String sql =  "INSERT INTO User VALUES(?,?,?,?)";
        PreparedStatement saveUser = connection.prepareStatement(sql);
        saveUser.setObject(1, stored.identity);
        saveUser.setObject(2, stored.version);
        saveUser.setString(3, user.name);
        saveUser.setString(4, user.joined.toString());
        saveUser.executeUpdate();
        return stored;
    }
    
    @Override
    public synchronized Stored<User> update(Stored<User> user,
                                            User new_user)
        throws UpdatedException,
            DeletedException,
            SQLException {
        final Stored<User> current = get(user.identity);
        final Stored<User> updated = current.newVersion(new_user);
        if(current.version.equals(user.version)) {
            String sql = "UPDATE User SET (version,name,joined) =(?,?,?) WHERE id=?";
            PreparedStatement updateUser = connection.prepareStatement(sql);
            updateUser.setObject(1, updated.version);
            updateUser.setString(2, new_user.name);
            updateUser.setString(3, new_user.joined.toString());
            updateUser.setObject(4, updated.identity);
            updateUser.executeUpdate();

        } else {
            throw new UpdatedException(current);
        }
        return updated;
    }
   
    @Override
    public synchronized void delete(Stored<User> user)
       throws UpdatedException,
              DeletedException,
              SQLException {
        final Stored<User> current = get(user.identity);
        if(current.version.equals(user.version)) {
            String sql =  "DELETE FROM User WHERE id = ?";
            PreparedStatement deleteUser = connection.prepareStatement(sql);
            deleteUser.setObject(1, user.identity);
            deleteUser.executeUpdate();
        } else {
        throw new UpdatedException(current);
        }
    }
    @Override
    public Stored<User> get(UUID id)
      throws DeletedException,
             SQLException {
        final String sql = "SELECT version,name,joined FROM User WHERE id = ?";
        PreparedStatement getUser = connection.prepareStatement(sql);
        getUser.setString(1, id.toString());
        final ResultSet rs = getUser.executeQuery();

        if(rs.next()) {
            final UUID version = 
                UUID.fromString(rs.getString("version"));
            final String name = rs.getString("name");
            final Instant joined = Instant.parse(rs.getString("joined"));
            return (new Stored<User>
                        (new User(name,joined),id,version));
        } else {
            throw new DeletedException();
        }
    }
    
    /**
     * Look up a user by their username;
     **/
    public Maybe<Stored<User>> lookup(String name) {
        final String sql = "SELECT id FROM User WHERE name = ?";
        try{
            PreparedStatement lookupUser = connection.prepareStatement(sql);
            lookupUser.setString(1, name);
            final ResultSet rs = lookupUser.executeQuery();
            if(rs.next())
                return Maybe.just(
                    get(UUID.fromString(rs.getString("id"))));
        } catch (Exception e) {
        
        }
        return Maybe.nothing();
    }
}


